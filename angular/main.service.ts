addHero (hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.createUrl, hero, httpOptions)
    .pipe(
      tap((hero: Hero) => this.log(`added hero w/ id=${hero.id}`)),
      catchError(this.handleError<Hero>('addHero'))
    );
  }

  getHeroes (): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.listUrl)
      .pipe(
        catchError(this.handleError('getHeroes', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }